describe('wish list testing',()=>{
    it('then:I should be able to add item to wish list',()=>{
        cy.visit('/');
        cy.get(':nth-child(3) > :nth-child(1) > .btn').click();
        cy.wait(2000);
        cy.get(':nth-child(1) > .nav-link').click();
        cy.get('tbody > tr > :nth-child(2)').should('contain.text','Mens Cotton Jacket');
        cy.wait(3000);


    })

    it('then: I should be able to delete item from wish list',()=>{
        cy.wait(1000);
        cy.get(':nth-child(5) > .btn').click();
        
        cy.get('#wishListEmpty').should('contain.text','Your Wish List Is Empty!');
        cy.wait(2000);
    })
})