describe('cart testing',()=>{
    it('then:I should be able to add product to cart', () => {
        
        cy.visit('/')
        cy.get(':nth-child(1) > #CartButtons').click();
        cy.get(':nth-child(2) > .nav-link').click();
        cy.url().should('includes', 'cart');
        cy.get('tbody > :nth-child(1) > :nth-child(2)').
            should('contain.text', 'Fjallraven - Foldsack No. 1 Backpack, Fits 15 Laptops');
        cy.wait(2000);


    })
    it('then:I should be able to delete product from cart', () => {
        cy.wait(2000);
        cy.get(':nth-child(7) > .btn').click();
       
        cy.get('#cartEmpty').should('contain.text', 'Your Cart Is Empty!');
        cy.wait(2000);
    })


})