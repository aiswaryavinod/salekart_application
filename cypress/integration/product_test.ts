describe('Product Testing', () => {

    it('then:I should be able to get all product', () => {
        cy.visit('/');
        cy.get('.row').should('have.length.greaterThan', 0);
        cy.wait(2000);
    })

    it('then:I should be able to view each product details', () => {
        cy.wait(2000);
        cy.visit('/')
        cy.get('.row > :nth-child(1) > img').click();
        cy.url().should('includes', 'view/1');
        cy.get('#productTitle').should('contain.text', 'Fjallraven - Foldsack No. 1 Backpack, Fits 15 Laptops')
        cy.wait(2000);
        cy.get('.btn').click();
        cy.wait(2000);
    })

 

    it('Then: I should be able to search the products', () => {
        cy.wait(2000);
        cy.visit('/')
        cy.get('input[type="text"]').type('Mens Cotton Jacket');
        cy.get('[data-testing="title"]').should('contain.text', 'Mens Cotton Jacket');
        cy.wait(2000);

    })




})
// npx cypress open