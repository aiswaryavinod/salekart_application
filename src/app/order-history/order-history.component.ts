import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../authentication.service';
import { ProductServiceService } from '../product-service.service';

@Component({
  selector: 'app-order-history',
  templateUrl: './order-history.component.html',
  styleUrls: ['./order-history.component.css']
})
export class OrderHistoryComponent implements OnInit {
  token: string;
  payload: string;
  username: any;
  str: any;
  u: any;
  orderDetails;
  len: any;

  constructor(private s: ProductServiceService, private auth: AuthenticationService) { }

  ngOnInit(): void {
    this.getAllOrders();

  }
  getAllOrders() {
    this.u = this.getUserInfo();
    
    this.s.getAllOrders(this.u).subscribe(
      data => {
        this.orderDetails = data;
        console.log(this.orderDetails);
        this.len = this.orderDetails.length;
      }
    )

  }

  getUserInfo() {
    this.token = this.auth.getToken();
    if (this.token) {

      this.payload = this.token.split(".")[1];
      this.payload = window.atob(this.payload);
      //console.log(this.payload);
      this.str = JSON.parse(this.payload);


      this.username = this.str.sub;

      return this.username;


    }
  }

}
