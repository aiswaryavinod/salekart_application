import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { user } from 'src/shared/user';
import { AuthenticationService } from '../authentication.service';
import { ProductServiceService } from '../product-service.service';

@Component({
  selector: 'app-update-user',
  templateUrl: './update-user.component.html',
  styleUrls: ['./update-user.component.css']
})
export class UpdateUserComponent implements OnInit {
  token: any;
  payload: any;
  str: any;
  username: any;



 
  

  userDetail:  user = new user();

  constructor(private s: ProductServiceService, private auth: AuthenticationService) { }

  ngOnInit(): void {

    this.getUserInfo();
  }

  getUserInfo() {
    this.userDetail= new user();
    this.token = this.auth.getToken();
    if (this.token) {

      this.payload = this.token.split(".")[1];
      this.payload = window.atob(this.payload);
      //console.log(this.payload);
      this.str = JSON.parse(this.payload);


      this.username = this.str.sub;
      //console.log(this.username);

      this.s.getUserByUserName(this.username).subscribe(
        data => {
          this.userDetail = data;
         
         console.log(this.userDetail);
        }
      )
    }


  }

  update(){
    console.log(this.userDetail);

    this.s.updateUser(this.username,this.userDetail).subscribe(
      data=>{
        window.location.href="/profile"
      }
    )
  }

  
}
