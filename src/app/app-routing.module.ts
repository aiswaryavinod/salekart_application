import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CartComponent } from './cart/cart.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { GuardGuard } from './guard.guard';
import { LoginComponent } from './login/login.component';

import { ProductComponent } from './product/product.component';

import { RegisterComponent } from './register/register.component';
import { ViewComponent } from './view/view.component';
import { WishListComponent } from './wish-list/wish-list.component';



const routes: Routes = [
  { path: 'product', component: ProductComponent },
  { path: 'cart', component: CartComponent,canActivate:[GuardGuard] },
  { path: 'list', component: WishListComponent,canActivate:[GuardGuard] },
  { path: 'checkout', component: CheckoutComponent,canActivate:[GuardGuard]  },
  { path: 'product/:id', component: ViewComponent },


  { path: 'login', component: LoginComponent },

  { path: 'register', component: RegisterComponent },

  { path: '', redirectTo: 'product', pathMatch: 'full' },

 
  { path: 'profile', loadChildren: () => import('./profile/profile.module').then(m => m.ProfileModule),canActivate:[GuardGuard] },
  { path: 'update', loadChildren: () => import('./update-user/update-user.module').then(m => m.UpdateUserModule),canActivate:[GuardGuard] },
  { path: 'order', loadChildren: () => import('./order-history/order-history.module').then(m => m.OrderHistoryModule),canActivate:[GuardGuard] }
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
