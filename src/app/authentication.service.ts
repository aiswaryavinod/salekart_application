import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';



@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private http: HttpClient) { }

  generateToken(data) {
    return this.http.post("http://localhost:8091/salekart/token", data);
  }

  login(token) {
    localStorage.setItem("token", token);
    return true;
  }

  isLoggedIn() {
    let t = localStorage.getItem("token");
    if (t == null || t == undefined) {
      return false;
    }
    else {
      return true;
    }

  }

  logout() {
    localStorage.removeItem("token");
  }

  getToken() {
    return localStorage.getItem("token");
  }
}
