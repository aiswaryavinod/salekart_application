import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
// import { timeStamp } from 'console';
import { user } from 'src/shared/user';
import { ProductServiceService } from '../product-service.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  user: user = new user;
  errorMessage: any;
  err: boolean;

  constructor(private s: ProductServiceService, private fb: FormBuilder,private r:Router) { }

  ngOnInit(): void {
    this.registerForm = this.fb.group({
      name: ['', Validators.required],
      username: ['', Validators.required],
      emailid: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
      address: ['', Validators.required],
      country: ['', Validators.required],
      state: ['', Validators.required],
      zip: ['', Validators.required],

    });
  }

  register() {
    this.user.name = this.registerForm.value.name;
    this.user.username = this.registerForm.value.username;
    this.user.emailid = this.registerForm.value.emailid;
    this.user.password = this.registerForm.value.password;
    this.user.address = this.registerForm.value.address;
    this.user.country = this.registerForm.value.country;
    this.user.state = this.registerForm.value.state;
    this.user.zip = this.registerForm.value.zip;

    console.log(this.user);

    this.s.putUserDetails(this.user).subscribe(
      data => {
        console.log("submitted");
        this.r.navigate(['login']);
      },
      (error) => {
        this.errorMessage = error.error.message;
        this.err = true;
        console.log("aaaaaaaaaaa");
        console.log(error);
      }
    )



  }

  reset(){
    this.registerForm.reset();
  }

}
