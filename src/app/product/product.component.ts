import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { products } from 'src/shared/products';

import { AuthenticationService } from '../authentication.service';
import { ProductServiceService } from '../product-service.service';


@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})

export class ProductComponent implements OnInit {

  productList: products[];
  filterList: products[];
  singleProduct;
  searchedKeyword;
  errorMessage: any;
  err: boolean;

  token: string;
  payload: any;
  str: any;
  username: any;
  userDetail: any;
  u;

  constructor(private s: ProductServiceService, private r: Router,
    private snack: MatSnackBar, private auth: AuthenticationService) { }

  ngOnInit(): void {
    this.getAllProduct();


  }

  getAllProduct() {
    this.s.getAllProduct().subscribe(
      data => {
        this.productList = data;


        this.filterList = this.productList;



        console.log(this.filterList);
      },

      (error) => {
        this.errorMessage = error;
        this.err = true;
        console.log(error);
      }
    );
  }


  addToCart(item) {

    this.token = this.auth.getToken();


    if (this.token) {

      this.u = this.getUserInfo();

      this.s.addToCart(this.u, item).subscribe(
        data => {
          console.log("added");
          console.log(item);
          this.openSnackBar('Added Product To your Cart!', ' Cart');
        },
        (error) => {
          this.errorMessage = error;
          this.err = true;
          console.log(error);
        }
      )
    }

    else {
      this.r.navigate(['login']);
    }



  }



  filter(category) {

    this.filterList = this.productList.filter(
      (a: any) => {
        if (a.category == category || category == '') {
          return a;
        }
      })

  }



  plus(p) {
    p.quantity += 1;
  }

  minus(p) {
    if (p.quantity != 1) {
      p.quantity = p.quantity - 1;
    }
  }

  openSnackBar(message: string, action: string) {
    var sb = this.snack.open(message, action, {
      duration: 4000

    });
    sb.onAction().subscribe(() => {
      this.r.navigate(['cart']);
    }

    )
  }

  openSnackBar1(message: string, action: string) {
    var sb1 = this.snack.open(message, action, {
      duration: 4000
    });
    sb1.onAction().subscribe(() => {
      this.r.navigate(['list']);
    }

    )

  }



  sort(value) {

    //console.log(value);

    if (value == 'low') {
      this.filterList.sort((a: any, b: any) => {
        return (a.price) - (b.price);

      })
    }
    else if (value == 'high') {
      this.filterList.sort((a: any, b: any) => {
        return (b.price) - (a.price);
      })
    }
    else {
      this.getAllProduct();
    }

  }

  sort1(value) {
    if (value == 'low') {
      this.filterList.sort((a: any, b: any) => {
        return (a.rating) - (b.rating);
      }
      )
    }
    else if (value == 'high') {
      this.filterList.sort((a: any, b: any) => {

        return (b.rating) - (a.rating);
      })

    }
  }

  addToWishList(p) {

    
    this.token = this.auth.getToken();
    if (this.token) {

      this.u = this.getUserInfo();
      this.s.addToList(this.u,p).subscribe(
        data => {

          this.openSnackBar1('Added Product To Your Wish List!', 'Wish List');
        },

        (error) => {
          this.errorMessage = error;
          this.err = true;
         
         
         
        }
      )
    }
    else {
      this.r.navigate(['login']);
    }

  }

  getUserInfo() {
    this.token = this.auth.getToken();
    if (this.token) {

      this.payload = this.token.split(".")[1];
      this.payload = window.atob(this.payload);
      console.log(this.payload);
      this.str = JSON.parse(this.payload);


      this.username = this.str.sub;

      return this.username;


    }
  }


}
