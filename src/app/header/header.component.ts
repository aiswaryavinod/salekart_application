import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AuthenticationService } from '../authentication.service';
import { ProductServiceService } from '../product-service.service';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  loggedIn: boolean = false;
  token:string;
  payload;
  str;
  username;
  userDetail: any;
  name;

  constructor(private auth: AuthenticationService, private s: ProductServiceService) { }

  ngOnInit(): void {
    this.loggedIn = this.auth.isLoggedIn();
    //console.log(this.loggedIn);
    this.getName();

  }

  logout() {
    this.auth.logout();
    window.location.href = "/product";

   

  }

  getName(){
    this.token = this.auth.getToken();
    if (this.token) {

      this.payload = this.token.split(".")[1];
      this.payload = window.atob(this.payload);
      //console.log(this.payload);
      this.str = JSON.parse(this.payload);
     

      this.username = this.str.sub;
      //console.log(this.username);

      this.s.getUserByUserName(this.username).subscribe(
        data=>{
          this.userDetail = data;
          // console.log(this.userDetail);
        }
      )
    }
      
  }

  

}
