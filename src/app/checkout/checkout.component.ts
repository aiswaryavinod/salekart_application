import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { user } from 'src/shared/user';
import { AuthenticationService } from '../authentication.service';
import { ProductServiceService } from '../product-service.service';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {
  list;
  len;
  submitted: boolean = false;

  sum = 0;
  userData;
  result = [];
  checkoutForm: FormGroup;
  errorMessage: any;
  err: boolean = false;
  token: string;
  payload: any;
  str: any;
  username: any;
  u: any;
  userDetail: any;
  deleteList: any;
  constructor(private s: ProductServiceService, private fb: FormBuilder, private r: Router, private auth: AuthenticationService) { }

  ngOnInit(): void {

    this.getAllItem();
    this.getDetails();
  }

  getAllItem() {

    this.u = this.getUserInfo();

    this.s.getFromCart(this.u).subscribe(
      data => {
        this.list = data;
        this.len = this.list.length;
        // console.log(this.list);

        this.list.forEach((a: any) => {
          a.subT = a.price * a.quantity;
          this.sum += a.subT;
        });




      },
      (error) => {
        this.errorMessage = error;
        this.err = true;
        console.log(error);
      }
    )
  }

  purchase() {

    this.u = this.getUserInfo();

    this.s.purchase(this.u, this.list).subscribe(
      data => {
        this.deleteAll();
        this.submitted = true;
        

      },
      (error) => {
        this.errorMessage = error;
        this.err = true;
        console.log(error);
      }
    )

  }


  shop() {
    this.r.navigate(['product']);
  }

  reset() {
    this.checkoutForm.reset();
  }

  goToCart() {
    this.r.navigate(['cart']);
  }

  getUserInfo() {
    this.token = this.auth.getToken();
    if (this.token) {

      this.payload = this.token.split(".")[1];
      this.payload = window.atob(this.payload);
      //console.log(this.payload);
      this.str = JSON.parse(this.payload);


      this.username = this.str.sub;

      return this.username;


    }
  }

  getDetails() {


    this.s.getUserByUserName(this.username).subscribe(
      data => {
        this.userDetail = data;
        // console.log(this.userDetail);
      }
    )
  }

  deleteAll() {
    this.u = this.getUserInfo();
    this.s.getFromCart(this.u).subscribe(
      data => {
        this.deleteList = data;
        this.deleteList.forEach((a: any) => {
          this.delete(a);
        });
      }
    )
  }

  delete(item) {
    console.log(item);
    this.s.deleteFromCart(item.id).subscribe(
      data => {


        // window.location.reload();
      },
      error => {
        this.errorMessage = error;
        this.err = true;
        console.log(this.errorMessage);
      }
    )
  }
}
