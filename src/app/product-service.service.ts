import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { products } from 'src/shared/products';

@Injectable({
  providedIn: 'root'
})
export class ProductServiceService {

  
  constructor(private http: HttpClient) { }

  getAllProduct(): Observable<any> {
    return this.http.get<any>("http://localhost:8091/salekart/products").pipe(
      catchError(this.handleError)
    );
  }

 ///////////////////cart////////////

  addToCart(username,data) {

    return this.http.post<any>("http://localhost:8091/salekart/cart/"+username, data).pipe(
      catchError(this.handleError)
    );
  }

  getFromCart(username): Observable<any> {
    return this.http.get<any>("http://localhost:8091/salekart/cart/"+username).pipe(
      catchError(this.handleError)
    );
  }

  deleteFromCart(id) {
    return this.http.delete<any>("http://localhost:8091/salekart/cart/" + id).pipe(
      catchError(this.handleError)
    );
  }

  getById(id): Observable<any> {
    return this.http.get<any>("http://localhost:8091/salekart/order/" + id);
  }

  getProductById(id): Observable<any> {
    return this.http.get<any>("http://localhost:8091/salekart/products/" + id).pipe(
      catchError(this.handleError)
    );
  }

  updateCart(id, updatedBody) {

    return this.http.put<any>("http://localhost:8091/salekart/cart/" + id, updatedBody);
  }

  
////////////////////list///////////////
   addToList(username,item) {
    return this.http.post<any>("http://localhost:8091/salekart/list/"+username, item).pipe(
      catchError(this.handleError)
    );
  }


  getFromList(username): Observable<any> {
    return this.http.get<any>("http://localhost:8091/salekart/list/"+username).pipe(
      catchError(this.handleError));
  }

  deleteFromList(id): Observable<any> {
    return this.http.delete<any>("http://localhost:8091/salekart/list/" + id).pipe(
      catchError(this.handleError)
    );
  }





  putUserDetails(data): Observable<any> {
    return this.http.post<any>("http://localhost:8091/salekart/register", data).pipe(
      catchError(this.handleError)
    );

  }

  getUserByUserName(username): Observable<any>{
    return this.http.get<any>("http://localhost:8091/salekart/user/" + username).pipe(
      catchError(this.handleError)
    );
  }

  updateUser(username,updatedBody){
    return this.http.put<any>("http://localhost:8091/salekart/user/" + username, updatedBody);
  }

  purchase(username,data): Observable<any>{
    console.log(data);
    return this.http.post<any>("http://localhost:8091/salekart/order/"+ username, data).pipe(
      catchError(this.handleError)
    );
  }

  getAllOrders(username): Observable<any> {
    return this.http.get<any>("http://localhost:8091/salekart/orders/"+username).pipe(
      catchError(this.handleError)
    );
  }


  private handleError(error: HttpErrorResponse) {
    if (error.status === 0) {
      // A client-side or network error 
      console.error('An error occurred:', error.error);
    } else {
      // server side error

      console.error(
        `Backend returned code ${error.status}, body was: `, error.error);
    }

     return throwError(error);
   
  }
}



