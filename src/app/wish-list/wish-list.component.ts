import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { AuthenticationService } from '../authentication.service';
import { ProductServiceService } from '../product-service.service';

@Component({
  selector: 'app-wish-list',
  templateUrl: './wish-list.component.html',
  styleUrls: ['./wish-list.component.css']
})
export class WishListComponent implements OnInit {
  list;
  listLength;
  delList;
  errorMessage;
  err: boolean = false;
  token:string;
  payload: any;
  str: any;
  username: any;
  u;

  constructor(private s: ProductServiceService, private sb: MatSnackBar, private r: Router,
    private auth: AuthenticationService) { }

  ngOnInit(): void {
    this.getFromWishList();
  }

  getFromWishList() {
    this.u = this.getUserInfo();
    this.s.getFromList(this.u).subscribe(
      data => {
        this.list = data;
        this.listLength = this.list.length;
        console.log(this.list);

      },
      error => {
        this.errorMessage = error;
        this.err = true;
        console.log(this.errorMessage);
      }
    )
  }

  deleteFromList(l) {
    console.log(l);
    this.s.deleteFromList(l.id).subscribe(
      data => {
        this.getFromWishList();

      },
      error => {
        this.errorMessage = error;
        this.err = true;
        console.log(this.errorMessage);
      }
    )
  }

  add(item) {
   
    
      this.u = this.getUserInfo();
    

     
      this.s.addToCart(this.u,item).subscribe(
        data => {
          this.deleteFromList(item);
        },
        error => {
          this.errorMessage = error;
          this.err = true;
          console.log(this.errorMessage);
        }
      )
     

     

   

  }

  openSnackBar(message: string, action: string) {
    var sb = this.sb.open(message, action, {
      duration: 4000

    });
    sb.onAction().subscribe(() => {
      this.r.navigate(['cart']);
    }

    )
  }

  deleteAll() {
    this.u = this.getUserInfo();
    this.s.getFromList(this.u).subscribe(
      data => {
        this.delList = data;
        this.delList.forEach((a: any) => {
          this.deleteFromList(a);
        });
      }
    )
  }
  goToShop() {
    this.r.navigate(['product']);
  }

  getUserInfo() {
    this.token = this.auth.getToken();
    if (this.token) {

      this.payload = this.token.split(".")[1];
      this.payload = window.atob(this.payload);
      console.log(this.payload);
      this.str = JSON.parse(this.payload);


      this.username = this.str.sub;

      return this.username;


    }
  }

}
