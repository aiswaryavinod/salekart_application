import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductServiceService } from '../product-service.service';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {
  prodId;
  details;
  errorMessage: any;
  err: boolean=false;
  constructor(private s: ProductServiceService, private act: ActivatedRoute, private r: Router, private snack: MatSnackBar) { }

  ngOnInit(): void {
    this.prodId = this.act.snapshot.params['id'];
    this.getDetails(this.prodId);
  }

  getDetails(id) {
    this.s.getProductById(id).subscribe(
      data => {
        this.details = data;
        console.log(this.details);
      },
      (error) => {
        this.errorMessage = error;
        this.err = true;
        console.log(error);
      }
    )
  }

  back() {
    this.r.navigate(['product']);
  }


}
