import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { AuthenticationService } from '../authentication.service';
//import { cart } from 'src/shared/cart';
import { ProductServiceService } from '../product-service.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  cartList;
  len;
  p;
  sum = 0;
  saved: boolean = false;
  subT = 1;
  cl;
  deleteList;
  x;
  UpdatedData;
  subTotal;

  errorMessage: any;
  err: boolean;
  token: any;
  payload: string;
  str: any;
  username: any;
  u: any;
  constructor(private s: ProductServiceService, private r: Router, private snack: MatSnackBar,private auth:AuthenticationService) { }

  ngOnInit(): void {
    this.getItemFromCart();

  }

  getItemFromCart() {

    this.u = this.getUserInfo();
    this.s.getFromCart(this.u).subscribe(
      data => {
        this.cartList = data;

        console.log(this.cartList);

        this.len = this.cartList.length;

        this.cartList.forEach((a: any) => {
          this.p = a.price;
          a.subTotal = this.p * a.quantity;
          this.sum += a.subTotal;
        });



        //console.log(this.cartList);
        // console.log(this.sum);

      },
      error => {
        this.errorMessage = error;
        this.err = true;
        
      }
    )
  }



  plus(p, x) {
    p.quantity += x;

    this.s.getById(p.id).subscribe(
      data => {
        this.cl = data;
        console.log(this.cl);
         
        if (p.quantity == 0) {
          this.delete(this.cl);
        }

        this.cl.quantity = p.quantity;
        // this.cl.subT = this.cl.price * this.cl.quantity;
        this.sum += x * this.cl.price;
        console.log(this.sum);
      }
     )

    this.update(p);

  }



  update(item) {

    const newData = { ...item, quantity: item.quantity };


    this.s.updateCart(item.id, newData).subscribe(
      data => {
        console.log("updated");
      }
    )


  }

  delete(item) {
    console.log(item);
    this.s.deleteFromCart(item.id).subscribe(
      data => {


        window.location.reload();
      },
      error => {
        this.errorMessage = error;
        this.err = true;
        console.log(this.errorMessage);
      }
    )
  }

  deleteAll() {
    this.u = this.getUserInfo();
    this.s.getFromCart(this.u).subscribe(
      data => {
        this.deleteList = data;
        this.deleteList.forEach((a: any) => {
          this.delete(a);
        });
      }
    )
  }

  goToShop() {
    this.r.navigate(['product']);
  }


  add(p) {
    this.u = this.getUserInfo();
    this.s.addToList(this.u,p).subscribe(
      data => {
        // this.getFromWishList();
        this.delete(p);
      },
      error => {
        this.errorMessage = error;
        this.err = true;
        console.log(this.errorMessage);
      }
    )
  }
  
  checkOut(c) {
    //console.log(c);
    this.r.navigate(['checkout']);
  }

  getUserInfo() {
    this.token = this.auth.getToken();
    if (this.token) {

      this.payload = this.token.split(".")[1];
      this.payload = window.atob(this.payload);
      console.log(this.payload);
      this.str = JSON.parse(this.payload);


      this.username = this.str.sub;

      return this.username;


    }
  }


}
