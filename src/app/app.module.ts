import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProductComponent } from './product/product.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { FormsModule } from '@angular/forms';
import { CartComponent } from './cart/cart.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ViewComponent } from './view/view.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { WishListComponent } from './wish-list/wish-list.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { HeaderComponent } from './header/header.component';
 import { TokenInterceptor } from './token.interceptor';


 //import { TokenInterceptorService } from './token-interceptor.service';



@NgModule({
  declarations: [
    AppComponent,
    ProductComponent,

    CartComponent,
    ViewComponent,
    WishListComponent,
    CheckoutComponent,
    RegisterComponent,
    LoginComponent,
    LogoutComponent,
    HeaderComponent
   

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule, Ng2SearchPipeModule, FormsModule, ReactiveFormsModule,

    MatSnackBarModule, BrowserAnimationsModule


  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass:TokenInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
// {provide:HTTP_INTERCEPTORS,useClass:authInterceptor,multi:true}