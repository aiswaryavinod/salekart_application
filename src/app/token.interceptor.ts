import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthenticationService } from './authentication.service';

@Injectable(

)
export class TokenInterceptor implements HttpInterceptor {

  constructor(private auth: AuthenticationService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    let newReq;
    let token = this.auth.getToken();

    if (token) {
      //console.log(token);

      newReq = req.clone({

        setHeaders: {

          Authorization: `Bearer ${token}`
        }




      });

      return next.handle(newReq);
    }

    else {
      req = req.clone({
        setHeaders: {
          'Content-Type': 'application/json; charset=utf-8',
          Accept: 'application/json'
        }
      });
      return next.handle(req);
    }





  }
}