import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { user } from 'src/shared/user';
import { AuthenticationService } from '../authentication.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  userlogin: user = new user;
  loginForm: FormGroup;
  errorMessage: any;
  err: boolean;

  constructor(private r: Router, private fb: FormBuilder, private auth: AuthenticationService) { }



  ngOnInit(): void {
    this.loginForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }


  login() {
    this.userlogin.username = this.loginForm.value.username;
    this.userlogin.password = this.loginForm.value.password;

    console.log(this.userlogin);


    this.auth.generateToken(this.userlogin).subscribe(
      (data: any) => {
        //console.log(data.token);
        
        //logging in
        this.auth.login(data.token);

        
       window.location.href="/product"
        
      },
      (error) => {
        this.errorMessage = error.error.message;
        this.err = true;
        console.log(error);
      }
    )


  }
}
