import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../authentication.service';
import { ProductServiceService } from '../product-service.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  token: String;
  payload;
  str
  username;
  userDetail;
  constructor(private auth: AuthenticationService, private s: ProductServiceService, private r: Router) { }

  ngOnInit(): void {

    this.getUserInfo();
  }

  getUserInfo() {
    this.token = this.auth.getToken();
    if (this.token) {

      this.payload = this.token.split(".")[1];
      this.payload = window.atob(this.payload);
      //console.log(this.payload);
      this.str = JSON.parse(this.payload);


      this.username = this.str.sub;
      //console.log(this.username);

      this.s.getUserByUserName(this.username).subscribe(
        data => {
          this.userDetail = data;
          // console.log(this.userDetail);
        }
      )
    }

  }

  gotocart() {
    this.r.navigate(['cart']);
  }

  updateUser(){
    this.r.navigate(['update']);
  }

  goToOrders(){
    this.r.navigate(['order']);
  }
}
