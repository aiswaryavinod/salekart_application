export class products{
    id:number;
    title:string;
    description:string;
    price:number;
    category:string;
    rating:number;
    quantity:number;
    // subTotal:number;
    image:string;

}